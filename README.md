# Open Source EDA CAD enabling

[[_TOC_]]

A plenty of free open source tools for EDA (Electronics Design Automation) have already become available and are matured enough to start using them in real ASIC/FPGA workflows. Besides, those tools are very good for
studying ASICs/FPGAs development from the beginning. 
However, a lot of guides from official pages may be quite complicated for non-programmers and students, preventing 
to be used in real workflows. So this Gitlab project is intended to collect best and simplest guides, HOWTOs
and real work practice for enabling opensource EDA CADs, PDKs for ASIC and FPGA design workflows.

- Open EDA tools:
  * Open ASIC,FPGA,PCB tools: https://opencircuitdesign.com
  * Open FPGA tools: https://openfpga.readthedocs.io
  * Open ROAD project: https://github.com/The-OpenROAD-Project
- Open PDKs:
  * Open PDK libs: http://opencircuitdesign.com/open_pdks/index.html
  * ASAP7 7nm Predictive PDK: https://github.com/The-OpenROAD-Project/asap7
- A collection of above in Conda Litex-hub: https://anaconda.org/litex-hub/repo

## Open EDA Tooling Treats

Simply Python notebook runnning all neceesary Open EDA toolchain - A very good option for start!

Context: https://twitter.com/proppy/status/1466020384063430657

https://colab.research.google.com/drive/1ww2Upi_UD_I9Z8Xwt-1vFwsysNEiznMu

## Getting started

Let's start from [Open ROAD project](https://github.com/The-OpenROAD-Project) !!!

### Preinstallation requirements
- **Linux host** I use `Ubuntu 18.04` on my servers or [`Ubuntu 20.04` on workstation with win10+WSL2](https://stackoverflow.com/a/65717977) 
- **Anaconda(Miniconda)** installed on Linux Host https://docs.conda.io/en/latest/miniconda.html. 
- **KLayout with QT** properly installed and can be open in GUI mode (refer to [Klayout download and build page](https://www.klayout.de/build.html) and [QT GUI error fix](https://askubuntu.com/a/1163268))

```shell
sudo apt install --reinstall libqt5designer5 libqt5multimedia5 libqt5multimediawidgets5 \
   libqt5opengl5 libqt5printsupport5 libqt5sql5 libqt5xmlpatterns5 libqt5core5a libqt5gui5
sudo strip --remove-section=.note.ABI-tag /usr/./lib/x86_64-linux-gnu/libQt5Core.so.5
wget https://www.klayout.org/downloads/Ubuntu-20/klayout_0.27.8-1_amd64.deb
sudo dpkg -i klayout_0.27.8-1_amd64.deb
```

### Installation process

Create and activate conda environment at path `./conda-openeda` then install some packages explicitly specifing conda repositories:
 - `conda-forge`
 - `litex-hub` 

```shell
conda create -y -p ./conda-openeda
conda activate ./conda-openeda
conda install -y -c conda-forge -c litex-hub open_pdks.sky130a magic yosys iverilog gtkwaves \
   verilator vtr-gui vtr-optimized vtr-gui openroad
```
Conda completes installation on my workstation within ~15 minutes:
```shell

... many lines were skipped...

iverilog-11.0        | 2.5 MB    | ######################################################################################## | 100%
libuuid-2.32.1       | 28 KB     | ######################################################################################## | 100%
fonts-conda-ecosyste | 4 KB      | ######################################################################################## | 100%
ld_impl_linux-64-2.3 | 667 KB    | ######################################################################################## | 100%
binutils_linux-64-2. | 24 KB     | ######################################################################################## | 100%
make-4.3             | 507 KB    | ######################################################################################## | 100%
libzlib-1.2.11       | 59 KB     | ######################################################################################## | 100%
python_abi-3.7       | 4 KB      | ######################################################################################## | 100%
fonts-conda-forge-1  | 4 KB      | ######################################################################################## | 100%
tk-8.6.12            | 3.3 MB    | ######################################################################################## | 100%
icu-68.2             | 13.1 MB   | ######################################################################################## | 100%
xorg-xextproto-7.3.0 | 28 KB     | ######################################################################################## | 100%
xorg-libx11-1.6.12   | 919 KB    | ######################################################################################## | 100%
Preparing transaction: done
Verifying transaction: done
Executing transaction: done


╭─    /home/open-cads ······················· ✔  took 15m 20s   /home/open-cads/conda-openeda   with tafkos@  at 20:00:51 
╰─
```

### Run `sverv` example to test functionality

OpenROAD is one of the simplified flow to get GDS from RTL. To test installation use guide from https://openroad.readthedocs.io/en/latest/user/GettingStarted.html#running-a-design

#### 1. Clone repo with `OpenROAD-flow-scripts`
```shell
cd /home/open-cads
git clone https://github.com/The-OpenROAD-Project/OpenROAD-flow-scripts
```
#### 2. Run test RTL-to-GDS flow:
```shell
cd OpenROAD-flow-scripts/flow
make DESIGN_CONFIG=./designs/nangate45/swerv/config.mk
```

<details>
<summary>

___Expand here to resolve ERROR `Illegal option -o pipefail`___

</summary>


```
$  make DESIGN_CONFIG=./designs/nangate45/swerv/config.mk
[INFO][FLOW] Using platform directory ./platforms/nangate45
/bin/sh: 0: Illegal option -o pipefail
/bin/sh: 0: Illegal option -o pipefail
```

[Described here](https://stackoverflow.com/questions/54055549/linux-ubuntu-set-illegal-option-o-pipefail) and **Resolved by** replacing dash by bash as default shell using [guide from here](https://askubuntu.com/questions/1064773/how-can-i-make-bin-sh-point-to-bin-bash):

* Run following command
```shell
sudo dpkg-reconfigure dash
```
* and select the option not to use `/bin/dash` as the default shell.


</details>

#### 3. As a result after 30 minutes of process run you will get text report looks like:

<details>
<summary>

`./logs/nangate45/swerv/base/6_report.log`

</summary>

```log
==========================================================================
finish hold_violation_count
--------------------------------------------------------------------------
hold violation count 0

==========================================================================
finish report_tns
--------------------------------------------------------------------------
tns 0.00

==========================================================================
finish report_wns
--------------------------------------------------------------------------
wns 0.00

==========================================================================
finish report_worst_slack
--------------------------------------------------------------------------
worst slack 0.11

==========================================================================
finish report_clock_skew
--------------------------------------------------------------------------
Clock core_clock
Latency      CRPR       Skew
_154314_/CK ^
   0.85
_156787_/CK ^
   0.45      0.00       0.40


==========================================================================
finish report_power
--------------------------------------------------------------------------
Group                  Internal  Switching    Leakage      Total
                          Power      Power      Power      Power
----------------------------------------------------------------
Sequential             1.48e-02   3.47e-03   8.80e-04   1.92e-02  34.7%
Combinational          1.21e-02   2.09e-02   3.16e-03   3.61e-02  65.3%
Macro                  0.00e+00   0.00e+00   0.00e+00   0.00e+00   0.0%
Pad                    0.00e+00   0.00e+00   0.00e+00   0.00e+00   0.0%
----------------------------------------------------------------
Total                  2.69e-02   2.43e-02   4.04e-03   5.53e-02 100.0%
                          48.7%      44.0%       7.3%

==========================================================================
finish report_design_area
--------------------------------------------------------------------------
Design area 183646 u^2 9% utilization.

Elapsed time: 1:08.89[h:]min:sec. CPU time: user 63.42 sys 5.43 (99%). Peak memory: 1316564KB.
```
</details>

#### 4. To view GDS in KLayout use command:
```shell
klayout -nn platforms/nangate45/FreePDK45.lyt results/nangate45/swerv/base/6_final.gds
```


## Authors and acknowledgment
Biggest respect I send to all authors and users of opensource community making even complicated tools available for everyone!

## License
All projects are licensed as they are. Here I'm not going to store, share any source codes - mostly links.

## Project status
It is just a start... Anyone who want to help are welcome!
